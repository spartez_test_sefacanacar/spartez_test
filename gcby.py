#!/usr/bin/python3.5

import requests
import json
import sys

team_name = sys.argv[1]
top = sys.argv[2]
limit = sys.argv[3]
limint = int(limit)
counter = 0
total = 0
perc = 0
author = []
author_list = []
auth_last_check = []
json_str_beg = "{\"tutorials\": {"
json_str_body = ""
json_str_end = "}}"
counter_str = 0
counter_repo = 0

if limint > 50:
	print("Value of LIMIT can not be higher than 50 due to API")
	sys.exit()

r = requests.get('https://bitbucket.org/api/2.0/repositories/' + team_name)
json_data = json.loads(r.text)
for i in json_data['values']:
	r2 = requests.get('https://api.bitbucket.org/1.0/repositories/' + team_name + '/' + i['name'] + '/changesets?limit=' + limit)
	json_data2 = json.loads(r2.text)
	for k in json_data2:
		if 'changesets' not in k:
			continue
		total = total + json_data2['count']
		for j in json_data2['changesets']:
			author_raw = (j['raw_author']).split("<")
			if counter > 0 and i['name'] == name_old:
	    			counter = counter + 1
			else :
	    			counter = 1
			counter_str = str(counter)
			if author_raw[0] not in author_list:
				author_list.append(author_raw[0])
			name_old = i['name']    
		author.append(author_raw[0]+"_"+i['name']+"_"+counter_str)
author.sort()
counter = 0
for i in author_list:
	if counter != 0:
		perc = str(counter_repo / total * 100)
		json_str_body = json_str_body + "}, \"total_commits_share_percentage\": " + perc + "}, "
	counter = 0
	counter_repo = 0
	if i in auth_last_check:
		continue
	for j in author:
		parser = j.split("_")
		if i in j:
			if counter == 0:
				json_str_body = json_str_body + "\"" + i +"\" {\"git_repos\": {\"" + parser[1] + "\": " + parser[2]
				counter_repo = counter_repo +int( parser[2])
				counter = 1
			else:
				json_str_body = json_str_body + ", " + parser[1] + "\": " + parser[2]
				counter_repo = counter_repo + int(parser[2])
		auth_last_check.append(i)

perc = str(counter_repo / total * 100)
json_str_body = json_str_body + "}, \"total_commits_share_percentage\": " + perc + "}, "

json_final = json_str_beg + json_str_body + json_str_end

print(json_final)

